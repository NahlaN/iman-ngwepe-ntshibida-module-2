void main() {
  // declaring variables
  var name = "Iman Ngwepe-Ntshibida";
  var favoriteApp = "Easy Equities";
  var city = "Durban";
  // printing to console
  print('My name is $name.');
  print('I like the $favoriteApp App.');
  print('$city is my city.');
}
